#!/bin/bash

set -eu

psql -Xv ON_ERROR_STOP=1 "${GM_DBURL}" <<EOF

INSERT INTO app_public.settings(name, value)
VALUES('app-setting', to_jsonb('this is a setting value stored as json text'::text))
on conflict (name) do nothing;

EOF
