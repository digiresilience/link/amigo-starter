-- region Bootstrap
drop schema if exists app_public cascade;
alter default privileges revoke all on sequences from public;
alter default privileges revoke all on functions from public;

-- By default the public schema is owned by `postgres`; we need superuser privileges to change this :(
-- alter schema public owner to waterbear;
revoke all on schema public from public;
grant all on schema public to :DATABASE_OWNER;


create schema app_public;
grant usage on schema
    public,
    app_public
    to
    :DATABASE_VISITOR,
    app_admin,
    app_anonymous,
    app_user;

/**********/

drop schema if exists app_hidden cascade;
create schema app_hidden;
grant usage on schema app_hidden to :DATABASE_VISITOR;
alter default privileges in schema app_hidden grant usage, select on sequences to :DATABASE_VISITOR;

/**********/

alter default privileges in schema public, app_public, app_hidden grant usage, select on sequences to :DATABASE_VISITOR;
alter default privileges in schema public, app_public, app_hidden
    grant execute on functions to
    :DATABASE_VISITOR,
    app_admin,
    app_anonymous,
    app_user;

/**********/

drop schema if exists app_private cascade;
create schema app_private;


-- endregion
-- region UtilFunctions
create function app_private.tg__add_job() returns trigger as
$$
begin
    perform graphile_worker.add_job(tg_argv[0], json_build_object('id', NEW.id),
                                    coalesce(tg_argv[1], public.gen_random_uuid()::text));
    return NEW;
end;
$$ language plpgsql volatile
                    security definer
                    set search_path to pg_catalog, public, pg_temp;
comment on function app_private.tg__add_job() is
    E'Useful shortcut to create a job on insert/update. Pass the task name as the first trigger argument, and optionally the queue name as the second argument. The record id will automatically be available on the JSON payload.';

/* ------------------------------------------------------------------ */

create function app_private.tg__timestamps() returns trigger as
$$
begin
    NEW.created_at = (case when TG_OP = 'INSERT' then NOW() else OLD.created_at end);
    NEW.updated_at = (case
                          when TG_OP = 'UPDATE' and OLD.updated_at >= NOW()
                              then OLD.updated_at + interval '1 millisecond'
                          else NOW() end);
    return NEW;
end;
$$ language plpgsql volatile
                    set search_path to pg_catalog, public, pg_temp;
comment on function app_private.tg__timestamps() is
    E'This trigger should be called on all tables with created_at, updated_at - it ensures that they cannot be manipulated and that updated_at will always be larger than the previous updated_at.';

-- endregion

-- region Users, Sessions, and Accounts
/* ------------------------------------------------------------------ */
create table app_private.sessions
(
    id             uuid        not null default gen_random_uuid() primary key,
    user_id        uuid        not null,
    expires        timestamptz not null,
    session_token  text        not null,
    access_token   text        not null,
    created_at     timestamptz not null default now(),
    updated_at     timestamptz not null default now(),
    last_active_at timestamptz not null default now()
);

create unique index session_token on app_private.sessions(session_token);
create unique index access_token on app_private.sessions(access_token);

alter table app_private.sessions
    enable row level security;



/* ------------------------------------------------------------------ */

create function app_public.current_session_id() returns uuid as
$$
-- note the jwt.claims.session_id doesn't mean you have to use jwt, it is just where this function will always look for the session id.
select nullif(pg_catalog.current_setting('jwt.claims.session_id', true), '')::uuid;
$$ language sql stable;
comment on function app_public.current_session_id() is
    E'Handy method to get the current session ID.';

/*
 * A less secure but more performant version of this function would be just:
 *
 *  select nullif(pg_catalog.current_setting('jwt.claims.user_id', true), '')::int;
 *
 * The increased security of this implementation is because even if someone gets
 * the ability to run SQL within this transaction they cannot impersonate
 * another user without knowing their session_id (which should be closely
 * guarded).
 */
create function app_public.current_user_id() returns uuid as
$$
select user_id
from app_private.sessions
where id = app_public.current_session_id();
$$ language sql stable
                security definer
                set search_path to pg_catalog, public, pg_temp;
comment on function app_public.current_user_id() is
    E'Handy method to get the current user ID for use in RLS policies, etc; in GraphQL, use `currentUser{id}` instead.';
-- We've put this in public, but omitted it, because it's often useful for debugging auth issues.

/* ------------------------------------------------------------------ */

-- These are the user roles for our application
create type app_public.role_type as
    ENUM ('none','admin', 'user');

/* ------------------------------------------------------------------ */

create table app_public.users
(
    id             uuid                  not null default uuid_generate_v1mc() primary key,
    email          citext                not null,
    email_verified timestamptz,
    name           text                  not null,
    avatar         text,
    user_role      app_public.role_type  not null default 'none',
    is_active      boolean               not null default false,
    created_at     timestamptz           not null default now(),
    updated_at     timestamptz           not null default now(),
    created_by     text                  not null,
    constraint     users_email_validity  check (email ~* '^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+[.][A-Za-z]+$'),
    constraint     users_avatar_validity check (avatar ~ '^https?://[^/]+'),
    constraint     users_email_unique    unique (email)
);
comment on table app_public.users is
    E'A user who can log in to the application.';
comment on column app_public.users.id is
    E'Unique identifier for the user.';
comment on column app_public.users.email is
    E'The email address of the user.';
comment on column app_public.users.email_verified is
    E'The time at which the email address was verified';
comment on column app_public.users.name is
    E'Public-facing name (or pseudonym) of the user.';
comment on column app_public.users.avatar is
    E'Optional avatar URL.';
comment on column app_public.users.user_role is
    E'The role that defines the user''s privileges.';
comment on column app_public.users.is_active is
    E'If false, the user is not allowed to login or access the application';

alter table app_public.users
    enable row level security;

alter table app_private.sessions
    add constraint sessions_user_id_fkey foreign key ("user_id") references app_public.users on delete cascade;

create index on app_private.sessions (user_id);

-- app_public perms default
create policy access_self on app_public.users to app_anonymous using (id = app_public.current_user_id());

--create policy update_self on app_public.users for update using (id = app_public.current_user_id());
grant select on app_public.users to app_anonymous;
grant update (name, avatar) on app_public.users to :DATABASE_VISITOR, app_user;

-- app_public perms for app_admin
create policy access_all on app_public.users to app_admin using (true);
grant update (email, name, avatar, is_active, user_role) on app_public.users to app_admin;
grant select on app_public.users to app_admin;
grant insert (email, name, avatar, user_role, is_active, created_by) on app_public.users to app_admin;
grant update (email, name, avatar, user_role, is_active, created_by) on app_public.users to app_admin;

create trigger _100_timestamps
    before insert or update
    on app_public.users
    for each row
execute procedure app_private.tg__timestamps();

/* ------------------------------------------------------------------ */

create function app_public.current_user() returns app_public.users as
$$
select users.*
from app_public.users
where id = app_public.current_user_id();
$$ language sql stable;
comment on function app_public.current_user() is
    E'The currently logged in user (or null if not logged in).';

/* ------------------------------------------------------------------ */

create function app_public.logout() returns void as
$$
begin
    -- Delete the session
    delete from app_private.sessions where id = app_public.current_session_id();
    -- Clear the identifier from the transaction
    perform set_config('jwt.claims.session_id', '', true);
end;
$$ language plpgsql security definer
                    volatile
                    set search_path to pg_catalog, public, pg_temp;


/* ------------------------------------------------------------------ */

create table app_public.accounts
(
    id                   uuid                  not null default uuid_generate_v1mc() primary key,
    compound_id          text not null,
    user_id              uuid        not null,
    provider_type        text not null,
    provider_id          text not null,
    provider_account_id  text not null,
    refresh_token        text,
    access_token         text,
    access_token_expires timestamptz,
    created_at           timestamptz           not null default now(),
    updated_at           timestamptz           not null default now()
);

alter table app_public.accounts
    enable row level security;

alter table app_public.accounts
    add constraint accounts_user_id_fkey foreign key ("user_id") references app_public.users on delete cascade;

create unique index accounts_compound_id         on app_public.accounts(compound_id);
create index        accounts_provider_account_id on app_public.accounts(provider_account_id);
create index        accounts_provider_id         on app_public.accounts(provider_id);
create index        accounts_user_id             on app_public.accounts (user_id);

create policy access_self on app_public.accounts to app_anonymous using (user_id = app_public.current_user_id());

grant select on app_public.accounts to app_anonymous;
grant update (compound_id, provider_type, provider_id, provider_account_id, refresh_token, access_token, access_token_expires) on app_public.accounts to app_user;

create policy access_all on app_public.accounts to app_admin using (true);
grant update (compound_id, provider_type, provider_id, provider_account_id, refresh_token, access_token, access_token_expires) on app_public.accounts to app_admin;
grant select on app_public.accounts  to app_admin;
grant insert (user_id, compound_id, provider_type, provider_id, provider_account_id, refresh_token, access_token, access_token_expires) on app_public.accounts to app_admin;
grant update (compound_id, provider_type, provider_id, provider_account_id, refresh_token, access_token, access_token_expires) on app_public.accounts to app_admin;

create trigger _100_timestamps
    before insert or update
    on app_public.accounts
    for each row
execute procedure app_private.tg__timestamps();

-- endregion

-- region Create first user function

create or replace function app_public.create_first_user (user_email text, user_name text)
    returns setof app_public.users
as
$$
declare
    user_count int;
begin

    user_count := (select count(id) from app_public.users);

    if (user_count != 0) then
        raise exception 'Admin user already created';
    end if;

    return query insert into app_public.users (email, email_verified, name, user_role, is_active, created_by)
                        values (user_email, now(), user_name, 'admin', true, 'first user hook') returning *;
end ;
$$ LANGUAGE plpgsql VOLATILE
                    SECURITY DEFINER;


comment on function app_public.create_first_user(user_email text, user_name text) is
    E'Creates the first user with an admin role. Only possible when there are no other users in the database.';

grant execute on function app_public.create_first_user(user_email text, user_name text) to app_anonymous;

create function app_private.tg__first_user() returns trigger as
$$
declare
    user_count int;
begin
    user_count := (select count(id) from app_public.users);

    if (user_count = 0) then
      NEW.user_role  = 'admin';
    end if;
    return NEW;
end;
$$ language plpgsql volatile
                    set search_path to pg_catalog, public, pg_temp;
comment on function app_private.tg__first_user() is
    E'This trigger is called to ensure the first user created is an admin';


create trigger _101_first_user
    before insert
    on app_public.users
    for each row
execute procedure app_private.tg__first_user();
-- endregion

-- region Settings

create table app_public.settings
(
    id                   uuid                  not null default uuid_generate_v1mc() primary key,
    name                 text                  not null,
    value                jsonb,
    created_at           timestamptz           not null default now(),
    updated_at           timestamptz           not null default now()

);

create unique index setting_name on app_public.settings(name);

alter table app_public.settings
    enable row level security;

create policy access_all on app_public.settings to app_admin using (true);
grant update (name, value) on app_public.settings to app_admin;
grant select on app_public.settings  to app_admin;
grant insert (name, value) on app_public.settings to app_admin;

create trigger _100_timestamps
    before insert or update
    on app_public.settings
    for each row
execute procedure app_private.tg__timestamps();

-- endregion
