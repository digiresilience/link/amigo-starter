require("@digiresilience/eslint-config-amigo/patch/modern-module-resolution");
module.exports = {
  extends: [
    "@digiresilience/eslint-config-amigo/profile/browser",
    "@digiresilience/eslint-config-amigo/profile/cypress",
    "@digiresilience/eslint-config-amigo/profile/typescript",
  ],
  parserOptions: { tsconfigRootDir: __dirname },
  rules: {
    "@typescript-eslint/no-explicit-any": "off",
    "@typescript-eslint/no-namespace": "off",
    "import/no-extraneous-dependencies": [
      // enable this when  this is fixed
      // https://github.com/benmosher/eslint-plugin-import/pull/1696
      "off",
      {
        packageDir: [
          ".",
          "node_modules/@digiresilience/amigo",
          "node_modules/@digiresilience/amigo-dev",
        ],
      },
    ],
  },
};
