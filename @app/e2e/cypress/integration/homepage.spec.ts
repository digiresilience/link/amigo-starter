/// <reference types="Cypress" />

context("HomePage", () => {
  it("renders correctly", () => {
    // Setup
    cy.visit("/");

    // Action

    // Assertions
    // cy.url().should("equal", Cypress.config("baseUrl") + "/");
    cy.get("form").within(() => {
      cy.get("button").should("exist").contains("Sign in with GitLab");
    });
  });
});
