#!/usr/bin/env node

import { Command } from "commander";
import { startWithout } from "@digiresilience/montar";
import { migrateWrapper } from "@app/db";
import { loadConfig } from "@app/config";
import { genConf, listConfig } from "./config";
import { createTokenForTesting, generateJwks } from "./jwks";
import "@app/api/build/main/server";
import "@app/api/build/main/logger";
import "@app/worker/build/main";

const program = new Command();

export async function runServer(): Promise<void> {
  await startWithout(["worker"]);
}

export async function runWorker(): Promise<void> {
  await startWithout(["server"]);
}

program
  .command("config-generate")
  .description("Generate a sample JSON configuration file (to stdout)")
  .action(genConf);

program
  .command("config-help")
  .description("Prints the entire convict config ")
  .action(listConfig);

program
  .command("api")
  .description("Run the application api server")
  .action(runServer);

program
  .command("worker")
  .description("Run the worker to process jobs")
  .action(runWorker);

program
  .command("db <commands...>")
  .description("Run graphile-migrate commands with your app's config loaded.")
  .action(async (args) => {
    const config = await loadConfig();
    return migrateWrapper(args, config);
  });

program
  .command("gen-jwks")
  .description("Generate the JWKS")
  .action(generateJwks);

program
  .command("gen-testing-jwt")
  .description("Generate a JWT for the test suite")
  .action(createTokenForTesting);
program.parse(process.argv);
