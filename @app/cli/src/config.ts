import { generateConfig, printConfigOptions } from "@digiresilience/amigo";
import { loadConfigRaw } from "@app/config";

export const genConf = async (): Promise<void> => {
  const c = await loadConfigRaw();
  const generated = generateConfig(c);
  console.log(generated);
};

export const genSchema = async (): Promise<void> => {
  const c = await loadConfigRaw();
  console.log(c.getSchemaString());
};

export const listConfig = async (): Promise<void> => {
  const c = await loadConfigRaw();
  printConfigOptions(c);
};
