module.exports = {
  async redirects() {
    return [
      // this redirect / to /admin
      // for apps that only have an admin section, you're done
      // but if you need a different index page, then remove this
      { source: "/", destination: "/admin", permanent: true },
    ];
  },
  async rewrites() {
    return [
      /*
      {
        source: "/api/v1/:path*",
        destination: "http://localhost:3001/api/:path*",
      },
*/
      {
        source: "/api/v1/:path*",
        destination: "/api/proxy/:path*",
      },
      {
        source: "/graphql",
        destination: "/api/graphql",
      },
    ];
  },
};
