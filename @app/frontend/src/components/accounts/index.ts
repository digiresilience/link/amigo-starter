import AccountIcon from "@material-ui/icons/AccountTree";
import AccountList from "./AccountList";
import AccountEdit from "./AccountEdit";

export default {
  list: AccountList,
  edit: AccountEdit,
  icon: AccountIcon,
};
