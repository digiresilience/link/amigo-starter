import * as React from "react";
import { FC, useState } from "react";
import { useSelector } from "react-redux";
import SecurityIcon from "@material-ui/icons/Security";
import { useMediaQuery, Box } from "@material-ui/core";
import {
  useTranslate,
  //  DashboardMenuItem,
  MenuItemLink,
  MenuProps,
} from "react-admin";
import users from "../users";
import accounts from "../accounts";

import SubMenu from "./SubMenu";

type MenuName = "menuCatalog" | "menuUsers" | "menuCustomers";

const Menu: FC<MenuProps> = ({ onMenuClick, logout, dense = false }) => {
  const [state, setState] = useState({
    menuUsers: true,
  });
  const translate = useTranslate();
  const isXSmall = useMediaQuery((theme) => theme.breakpoints.down("xs"));
  const open = useSelector((state) => state.admin.ui.sidebarOpen);
  useSelector((state) => state.theme); // force rerender on theme change

  const handleToggle = (menu: MenuName) => {
    setState((state) => ({ ...state, [menu]: !state[menu] }));
  };

  //<DashboardMenuItem onClick={onMenuClick} sidebarIsOpen={open} />
  return (
    <Box mt={1}>
      {" "}
      <SubMenu
        handleToggle={() => handleToggle("menuUsers")}
        isOpen={state.menuUsers}
        sidebarIsOpen={open}
        name="pos.menu.security"
        icon={<SecurityIcon />}
        dense={dense}
      >
        <MenuItemLink
          to={`/users`}
          primaryText={translate(`resources.users.name`, {
            smart_count: 2,
          })}
          leftIcon={<users.icon />}
          onClick={onMenuClick}
          sidebarIsOpen={open}
          dense={dense}
        />
        <MenuItemLink
          to={`/accounts`}
          primaryText={translate(`resources.accounts.name`, {
            smart_count: 2,
          })}
          leftIcon={<accounts.icon />}
          onClick={onMenuClick}
          sidebarIsOpen={open}
          dense={dense}
        />
      </SubMenu>
      {isXSmall && logout}
    </Box>
  );
};

export default Menu;
