import React from "react";
import {
  List,
  Datagrid,
  ImageField,
  DateField,
  TextField,
  EmailField,
  BooleanField,
} from "react-admin";

const UserList = (props) => (
  <List {...props} exporter={false}>
    <Datagrid rowClick="edit">
      <EmailField source="email" />
      <DateField source="emailVerified" />
      <TextField source="name" />
      <ImageField source="avatar" />
      <TextField source="userRole" />
      <BooleanField source="isActive" />
      <DateField source="createdAt" />
      <DateField source="updatedAt" />
      <TextField source="createdBy" />
    </Datagrid>
  </List>
);

export default UserList;
