import React from "react";
import { SimpleForm, TextInput, BooleanInput, Create } from "react-admin";
import { useSession } from "next-auth/client";
import { UserRoleInput } from "./shared";

const UserCreate = (props) => {
  const [session] = useSession();
  return (
    <Create {...props} title="Create Users">
      <SimpleForm>
        <TextInput source="email" />
        <TextInput source="name" />
        <UserRoleInput session={session} initialValue="NONE" />
        <BooleanInput source="isActive" initialValue={true} />
        <TextInput source="createdBy" initialValue={session.user.name} />
      </SimpleForm>
    </Create>
  );
};

export default UserCreate;
