import React, { useEffect } from "react";
import { CircularProgress, Typography, Grid } from "@material-ui/core";
import { signIn, signOut, getSession } from "next-auth/client";
import { useLogin, useTranslate } from "react-admin";

export const authProvider = {
  login: (o) => {
    if (o.ok) return Promise.resolve();
    return Promise.reject();
  },
  logout: async () => {
    const session = await getSession();
    if (session) {
      await signOut();
    }
  },
  checkError: (e) => {
    if (e.graphQLErrors && e.graphQLErrors.length > 0) {
      const permDenied =
        e.graphQLErrors.filter((e) => e.message.match(/.*permission denied.*/))
          .length >= 1;
      if (permDenied) return Promise.reject();
    }

    if (e.networkError) {
      if (e.networkError.statusCode === 401) return Promise.reject();
    }
  },
  checkAuth: async () => {
    const session = await getSession();
    if (!session) {
      return Promise.reject();
    }
  },
  getIdentity: async () => {
    const session = await getSession();
    if (!session) return Promise.reject(new Error("Invalid session"));

    return {
      fullName: session.user.name,
      avatar: session.user.image,
    };
  },
  getPermissions: () => Promise.resolve(),
};

export const AdminLogin = () => {
  const reactAdminLogin = useLogin();
  const translate = useTranslate();

  useEffect(async () => {
    const session = await getSession();
    if (!session) {
      signIn();
    } else {
      reactAdminLogin({ ok: true });
    }
  });

  return (
    <Grid
      container
      spacing={5}
      direction="column"
      alignItems="center"
      justify="center"
      style={{ minHeight: "100vh" }}
    >
      <Grid item xs={3}>
        <Typography variant="h4" color="textSecondary">
          {translate("auth.loggingIn")}
        </Typography>
      </Grid>
      <Grid item xs={3}>
        <CircularProgress size={80} />
      </Grid>
    </Grid>
  );
};
