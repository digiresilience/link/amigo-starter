import { Formik, Form, Field } from "formik";
import { TextField } from "formik-material-ui";
import { Grid, Button, Typography } from "@material-ui/core";
import { useRouter } from "next/router";

export default function Create() {
  const router = useRouter();

  return (
    <div>
      <h1>Add User</h1>
      <Formik
        initialValues={{ email: "", displayName: "" }}
        validate={(values) => {
          return null;
        }}
        onSubmit={async (values, { setSubmitting }) => {
          const result = await fetch("/api/v1/users", {
            method: "POST",
            body: JSON.stringify(values),
            headers: { "Content-Type": "application/json" },
          });
          setSubmitting(false);
          router.push("/users");
        }}
      >
        {({ submitForm, isSubmitting }) => (
          <Form>
            <Grid container direction="column" spacing={5}>
              <Grid item>
                <Typography variant="h6">Email Address</Typography>
                <Field
                  component={TextField}
                  name="email"
                  type="text"
                  label="Email Address"
                  fullWidth
                />
              </Grid>
              <Grid item>
                <Typography variant="h6">Display Name</Typography>
                <Field
                  component={TextField}
                  name="displayName"
                  type="text"
                  label="Display Name"
                  fullWidth
                />
              </Grid>
              <Grid item>
                <Grid container direction="row-reverse">
                  <Grid item>
                    <Button variant="contained" onClick={submitForm}>
                      Save
                    </Button>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Form>
        )}
      </Formik>
    </div>
  );
}
