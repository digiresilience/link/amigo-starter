import {
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableBody,
  TableCell,
  Paper,
  Button,
  Grid,
} from "@material-ui/core";
import Link from "next/link";
import useSWR from "swr";

export default function Users() {
  const { data, error } = useSWR("/api/v1/users");
  if (error) return <div>failed to load</div>;
  if (!data) return <div>loading...</div>;

  const rows = data.data;

  return (
    <div>
      <h1>Users</h1>
      <Grid container direction="row-reverse" style={{ marginBottom: 20 }}>
        <Grid item>
          <Link href={`/users/create`}>
            <Button variant="contained">Add</Button>
          </Link>
        </Grid>
      </Grid>
      <TableContainer component={Paper}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>ID</TableCell>
              <TableCell>Email Address</TableCell>
              <TableCell>Name</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row: any) => (
              <TableRow key={row.id}>
                <TableCell component="th" scope="row">
                  {row.id}
                </TableCell>
                <TableCell>{row.email}</TableCell>
                <TableCell>{row.name}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
}
