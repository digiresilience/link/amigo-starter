import "../styles/globals.css";
import { Provider } from "next-auth/client";
import { SWRConfig } from "swr";

function AmigoStarter({ Component, pageProps }) {
  return (
    <Provider session={pageProps.session}>
      <SWRConfig
        value={{
          refreshInterval: 10000,
          fetcher: (...args) => fetch(...args).then((res) => res.json()),
        }}
      >
        <Component {...pageProps} />
      </SWRConfig>
    </Provider>
  );
}

export default AmigoStarter;
