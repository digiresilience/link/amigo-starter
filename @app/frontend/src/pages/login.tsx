import { Button } from "@material-ui/core";
import { signIn, signOut, useSession } from "next-auth/client";

export default function myComponent() {
  const [session, loading] = useSession();

  return (
    <>
      {!session && (
        <>
          Not signed in <br />
          <Button variant="contained" onClick={signIn as any}>
            Sign in
          </Button>
        </>
      )}
      {session && (
        <>
          Signed in as {session.user.email} <br />
          <Button variant="contained" onClick={signOut as any}>
            Sign out
          </Button>
        </>
      )}
    </>
  );
}
