import { ApolloProvider } from "@apollo/client";
import { apolloClient } from "../lib/apollo-client";
import AmigoAdmin from "../components/AmigoAdmin";

export default function Home() {
  return (
    <ApolloProvider client={apolloClient}>
      <AmigoAdmin />
    </ApolloProvider>
  );
}
