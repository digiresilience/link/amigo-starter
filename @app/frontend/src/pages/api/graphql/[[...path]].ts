import { createProxyMiddleware } from "http-proxy-middleware";

export default createProxyMiddleware({
  target: "http://localhost:3001",
  changeOrigin: true,
  pathRewrite: { "^/graphql": "/graphql" },
  xfwd: true,
  onProxyReq: function (proxyReq, req, _res) {
    const auth = proxyReq.getHeader("authorization");
    if (auth) {
      // pass along user provided authorization header
      return;
    }

    // Else extract the session token from the cookie and pass
    // as bearer token to the proxy target
    let token = req.cookies["__Secure-next-auth.session-token"];
    if (!token) token = req.cookies["next-auth.session-token"];

    //console.log(req.body);
    //if (req.body.query) console.log(req.body.query);
    if (token) {
      proxyReq.setHeader("authorization", `Bearer ${token}`);
      proxyReq.removeHeader("cookie");
    } else {
      console.error("no token found. proxied request to backend will fail.");
    }
  },
});

export const config = {
  api: {
    bodyParser: false,
  },
};
