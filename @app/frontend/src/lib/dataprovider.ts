import pgDataProvider from "ra-postgraphile";

const specialThing = async (type, resource, params) => {
  if (type === "GET_LIST") {
    return {
      data: [
        { id: "1", name: "special" },
        { id: "2", name: "more special" },
      ],
      total: 2,
    };
  }
};

export const amigoDataProvider = async (client) => {
  const graphqlDataProvider = await pgDataProvider(client);

  const dataProvider = async (type, resource, params) => {
    // this is an example of how to override certain calls to the data provider
    if (resource === "specialThing")
      return specialThing(type, resource, params);

    // otherwise it falls back to the graphql data provider
    return graphqlDataProvider(type, resource, params);
  };

  return dataProvider;
};
