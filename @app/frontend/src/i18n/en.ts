import { TranslationMessages } from "react-admin";
import englishMessages from "ra-language-english";

const customEnglishMessages: TranslationMessages = {
  ...englishMessages,

  auth: {
    loggingIn: "Logging in...",
  },
  pos: {
    configuration: "Configuration",
    menu: {
      security: "Security",
      accounts: "Accounts",
    },
  },
  resources: {
    users: {
      name: "User |||| Users",
    },
    accounts: {
      name: "Account |||| Accounts",
    },
  },
  validation: {},
};

export default customEnglishMessages;
