import * as Glue from "@hapi/glue";
import * as Amigo from "@digiresilience/amigo";
import * as Blipp from "blipp";
import HapiBasic from "@hapi/basic";
import HapiJwt from "hapi-auth-jwt2";
import HapiPostgraphile from "hapi-postgraphile";
import PgSimplifyInflectorPlugin from "@graphile-contrib/pg-simplify-inflector";
// import PgManyToManyPlugin from "@graphile-contrib/pg-many-to-many";
import ConnectionFilterPlugin from "postgraphile-plugin-connection-filter";
import AppPlugin from "../app";
import type { IAppConfig } from "../config";

const build = async (config: IAppConfig): Promise<Glue.Manifest> => {
  const { port, address } = config.server;
  const amigoPlugins = Amigo.defaultPlugins(config);
  return {
    server: {
      port,
      address,
      debug: false, // We use pino not the built-in hapi logger
      routes: {
        validate: {
          failAction: Amigo.validatingFailAction,
        },
      },
    },
    register: {
      plugins: [
        // jwt plugin, required for our jwt auth plugin
        { plugin: HapiJwt },

        // Blipp prints the nicely formatted list of endpoints at app boot
        { plugin: Blipp },

        // load the amigo base plugins
        ...amigoPlugins,

        // basic authentication, required by hapi-nextauth
        { plugin: HapiBasic },

        // load our main app
        {
          plugin: AppPlugin,
          options: {
            config,
          },
        },
        // load Postgraphile
        {
          plugin: HapiPostgraphile,
          options: {
            route: {
              path: "/graphql",
              options: {
                auth: {
                  strategies: ["nextauth-jwt"],
                  mode: "optional",
                },
              },
            },
            pgConfig: config.postgraphile.authConnection,
            schemaName: "app_public",
            schemaOptions: {
              jwtAudiences: [config.nextAuth.audience],
              jwtSecret: "",
              // unauthenticated users will hit the database with this role
              pgDefaultRole: "app_anonymous",
              ignoreRBAC: false,
              dynamicJson: true,
              ignoreIndexes: false,
              appendPlugins: [
                PgSimplifyInflectorPlugin,
                // PgManyToManyPlugin,
                ConnectionFilterPlugin,
              ],
            },
          },
        },
      ],
    },
  };
};

const Manifest = {
  build,
};

export default Manifest;
