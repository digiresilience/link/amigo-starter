/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { Prometheus } from "@promster/hapi";
import { startWithout, startOnly, stop } from "@digiresilience/montar";
import { migrateWrapper } from "@app/db";
import { stopDeployment, deployment } from "./server";
import config, { loadConfig } from "@app/config";

export const initServer = async () => {
  await startWithout(["server", "worker"]);
  return deployment(config, false);
};

export const resetServer = async (hapi) => {
  await stopDeployment(hapi);
  await stop();
  Prometheus.register.clear();
};

export const readyDb = async () => {
  await startOnly(["config", "db"]);
};

export const resetDb = async () => {
  const config = await loadConfig();
  try {
    await migrateWrapper(["reset", "--erase"], config, true);
    await migrateWrapper(["migrate"], config, false);
    await migrateWrapper(["watch", "--once"], config, true);
  } catch (error) {
    console.error(error);
    throw new Error("Database could not be reset for test suite");
  }
};

export const readyConfig = async () => {
  return startOnly(["config"]);
};

export const resetConfig = async () => {
  return stop();
};

export const JWT_ADMIN =
  "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJUZXN0IEVudiIsImlhdCI6MTYwNjg5Mzk2MCwiYXVkIjoiYW1pZ28tc3RhcnRlciIsInN1YiI6InRlc3RlckBleGFtcGxlLmNvbSIsIm5hbWUiOiJUZXN0ZXIgTWNHZWUiLCJlbWFpbCI6InRlc3RlckBleGFtcGxlLmNvbSIsInVzZXJSb2xlIjoiYWRtaW4iLCJleHAiOjQ3NjI2NTM5NjB9.PbRgcEvaiO_P49Gybk__C36GpCd64iyAIpkOGnaZ4Dx9K5M81xvjjD83Vm9k_0yxJgzpxBUY2agjE4MASBbzbg";

export const AUTH_ADMIN = `Bearer ${JWT_ADMIN}`;
