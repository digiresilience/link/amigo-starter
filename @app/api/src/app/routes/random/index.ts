import * as Hapi from "@hapi/hapi";
import * as Helpers from "../helpers";
import { Prometheus } from "@promster/hapi";

export const RandomNumberRoute = Helpers.withDefaults({
  method: "get",
  path: "/api/number",
  options: {
    description: "Get a random number",
    handler: async (request: Hapi.Request, _h: Hapi.ResponseToolkit) => {
      const { randomService } = request.services();
      const result = await randomService.generateInteger();

      // with the pino logger the first arg is an object of data to log
      // the second arg is a message
      // all other args are formated args for the msg
      request.logger.info(
        { result },
        "served a random number at %s",
        new Date()
      );

      return { result };
    },
  },
});

export const RandomStringRoute = async (
  _server: Hapi.Server
): Promise<Hapi.ServerRoute[]> => {
  // example of a custom prometheus metric
  const randStrCounter = new Prometheus.Counter({
    name: "amigo_starter_random_strings_served",
    help: "The number of random strings served",
  });

  return Helpers.withDefaults({
    method: "get",
    path: "/api/string",
    options: {
      description: "Get a random string",
      handler: async (request: Hapi.Request, _h: Hapi.ResponseToolkit) => {
        const { randomService } = request.services();
        const result = await randomService.generateString();

        randStrCounter.inc();
        return { result };
      },
    },
  });
};
