import * as Joi from "joi";
import * as Hapi from "@hapi/hapi";
import {
  UserRecord,
  crudRoutesFor,
  CrudControllerBase,
} from "@digiresilience/amigo";
import * as Helpers from "../helpers";

class UserRecordController extends CrudControllerBase(UserRecord) {}

const validator = (): Record<string, Hapi.RouteOptionsValidate> => ({
  create: {
    payload: Joi.object({
      name: Joi.string().required(),
      email: Joi.string().email().required(),
      emailVerified: Joi.string().isoDate().required(),
      createdBy: Joi.string().required(),
      avatar: Joi.string()
        .uri({ scheme: ["http", "https"] })
        .optional(),
      userRole: Joi.string().optional(),
      isActive: Joi.boolean().optional(),
    }).label("UserCreate"),
  },
  updateById: {
    params: {
      userId: Joi.string().uuid().required(),
    },
    payload: Joi.object({
      name: Joi.string().optional(),
      email: Joi.string().email().optional(),
      emailVerified: Joi.string().isoDate().optional(),
      createdBy: Joi.boolean().optional(),
      avatar: Joi.string()
        .uri({ scheme: ["http", "https"] })
        .optional(),
      userRole: Joi.string().optional(),
      isActive: Joi.boolean().optional(),
      createdAt: Joi.string().isoDate().optional(),
      updatedAt: Joi.string().isoDate().optional(),
    }).label("UserUpdate"),
  },
  deleteById: {
    params: {
      userId: Joi.string().uuid().required(),
    },
  },
  getById: {
    params: {
      userId: Joi.string().uuid().required(),
    },
  },
});

export const UserRoutes = async (
  _server: Hapi.Server
): Promise<Hapi.ServerRoute[]> => {
  const controller = new UserRecordController("users", "userId");
  return Helpers.withDefaults(
    crudRoutesFor("user", "/api/users", controller, "userId", validator())
  );
};
