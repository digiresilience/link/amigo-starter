import * as Amigo from "@digiresilience/amigo";
import Toys from "toys";

export const withDefaults = Toys.withRouteDefaults({
  options: {
    cors: true,
    auth: "nextauth-jwt",
    validate: {
      failAction: Amigo.validatingFailAction,
    },
  },
});
