import type { IMain } from "pg-promise";
import type RandomService from "../services/random";
import type { ISettingsService } from "../services/settings";
import type { IAppConfig } from "../../config";
import type { AppDatabase } from "../db";

// add your service interfaces here
interface AppServices {
  randomService: RandomService;
  settingsService: ISettingsService;
}

// extend the hapi types with our services and config
declare module "@hapi/hapi" {
  export interface Request {
    services(): AppServices;
    db(): AppDatabase;
    pgp: IMain;
  }
  export interface Server {
    config(): IAppConfig;
    db(): AppDatabase;
    pgp: IMain;
  }
}
