import * as Inert from "@hapi/inert";
import * as Vision from "@hapi/vision";
import type * as Hapi from "@hapi/hapi";
import * as HapiSwagger from "hapi-swagger";

export const registerSwagger = async (server: Hapi.Server): Promise<void> => {
  const swaggerOptions: HapiSwagger.RegisterOptions = {
    info: {
      title: "Amigo Starter API Docs",
      description: "Your friendly http web starter",
      version: "0.1",
    },
    // group sets of endpoints by tag
    tags: [
      {
        name: "users",
        description: "API for Users",
      },
    ],
    documentationRouteTags: ["swagger"],
    documentationPath: "/api-docs",
  };

  await server.register([
    { plugin: Inert },
    { plugin: Vision },
    {
      plugin: HapiSwagger,
      options: swaggerOptions,
    },
  ]);
};
