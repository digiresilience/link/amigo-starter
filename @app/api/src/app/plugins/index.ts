import type * as Hapi from "@hapi/hapi";
import Schmervice from "@hapipal/schmervice";
import PgPromisePlugin from "@digiresilience/hapi-pg-promise";

import type { IAppConfig } from "../../config";
import { dbInitOptions } from "../db";
import { registerNextAuth } from "./hapi-nextauth";
import { registerSwagger } from "./swagger";
import { registerNextAuthJwt } from "./nextauth-jwt";
import { registerCloudflareAccessJwt } from "./cloudflare-jwt";

export const register = async (
  server: Hapi.Server,
  config: IAppConfig
): Promise<void> => {
  await server.register(Schmervice);

  await server.register({
    plugin: PgPromisePlugin,
    options: {
      // the only required parameter is the connection string
      connection: config.db.connection,
      // ... and the pg-promise initialization options
      pgpInit: dbInitOptions(config),
    },
  });

  await registerNextAuth(server, config);
  await registerSwagger(server);
  await registerNextAuthJwt(server, config);
  await registerCloudflareAccessJwt(server, config);
};
