import camelcaseKeys from "camelcase-keys";
import type { IAppConfig } from "../config";
import {
  UserRecordRepository,
  AccountRecordRepository,
  SessionRecordRepository,
} from "@digiresilience/amigo";
import { SettingRecordRepository } from "@app/db";
import type { IInitOptions, IDatabase } from "pg-promise";

export interface IRepositories {
  users: UserRecordRepository;
  sessions: SessionRecordRepository;
  accounts: AccountRecordRepository;
  settings: SettingRecordRepository;
}

export type AppDatabase = IDatabase<IRepositories> & IRepositories;

export const dbInitOptions = (
  _config: IAppConfig
): IInitOptions<IRepositories> => {
  return {
    noWarnings: true,
    receive(data, result) {
      result.rows = camelcaseKeys(data);
    },

    // Extending the database protocol with our custom repositories;
    // API: http://vitaly-t.github.io/pg-promise/global.html#event:extend
    extend(obj: AppDatabase, _dc) {
      // Database Context (_dc) is mainly needed for extending multiple databases with different access API.

      // NOTE:
      // This event occurs for every task and transaction being executed (which could be every request!)
      // so it should be as fast as possible. Do not use 'require()' or do any other heavy lifting.
      obj.users = new UserRecordRepository(obj);
      obj.sessions = new SessionRecordRepository(obj);
      obj.accounts = new AccountRecordRepository(obj);
      obj.settings = new SettingRecordRepository(obj);
    },
  };
};
