import * as Hapi from "@hapi/hapi";
import * as Schmervice from "@hapipal/schmervice";
import { settingInfo, SettingsService } from "@app/db";

export const VoicemailPrompt = settingInfo<string>("voicemail-prompt");
export const VoicemailMinLength = settingInfo<number>("voicemail-min-length");
export const VoicemailUseTextPrompt = settingInfo<boolean>(
  "voicemail-use-text-prompt"
);

export { ISettingsService } from "@app/db";

const service = (server: Hapi.Server): Schmervice.ServiceFunctionalInterface =>
  SettingsService(server.db().settings);

export default service;
