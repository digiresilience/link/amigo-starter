import * as Worker from "graphile-worker";
import { defState } from "@digiresilience/montar";
import config from "./config";

const startWorkerUtils = async (): Promise<Worker.WorkerUtils> => {
  const workerUtils = await Worker.makeWorkerUtils({
    connectionString: config.worker.connection,
  });
  return workerUtils;
};

const stopWorkerUtils = async (): Promise<void> => {
  return workerUtils.release();
};

const workerUtils = defState("apiWorkerUtils", {
  start: startWorkerUtils,
  stop: stopWorkerUtils,
});

export default workerUtils;
