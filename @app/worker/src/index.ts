import * as Worker from "graphile-worker";
import { defState } from "@digiresilience/montar";
import config from "@app/config";
import logger from "./logger";
import workerUtils from "./utils";

const logFactory = (scope) => (level, message, meta) => {
  const pinoLevel = level === "warning" ? "warn" : level;
  const childLogger = logger.child({ scope });
  if (meta) childLogger[pinoLevel](meta, message);
  else childLogger[pinoLevel](message);
};

export const configWorker = async (): Promise<Worker.RunnerOptions> => {
  const { concurrency, pollInterval } = config.worker;
  logger.info({ concurrency, pollInterval }, "Starting worker");
  return {
    concurrency,
    pollInterval,
    logger: new Worker.Logger(logFactory),
    connectionString: config.worker.connection,
    taskDirectory: `${__dirname}/tasks`,
  };
};

export const startWorker = async (): Promise<Worker.Runner> => {
  await workerUtils.migrate();
  logger.info("migrated");
  const workerConfig = await configWorker();
  const worker = await Worker.run(workerConfig);
  return worker;
};

export const stopWorker = async (): Promise<void> => {
  return worker.stop();
};

const worker = defState("worker", {
  start: startWorker,
  stop: stopWorker,
});

export default worker;
