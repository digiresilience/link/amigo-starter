import { JobHelpers } from "graphile-worker";

interface HelloTaskOptions {
  name: string;
}

const helloTask = async (
  payload: HelloTaskOptions,
  helpers: JobHelpers
): Promise<void> => {
  const { name } = payload;
  helpers.logger.info(`Hello, ${name}!`);
};

export default helloTask;
