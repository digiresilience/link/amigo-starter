# amigo-starter

Your friendly but opinonated application template.

Use it for your next node.js web application. Built with Next.js, Hapi, convict,
graphile-{migrate,worker}, and many more.

Amigo is designed with the following goals:

- to provide a standard kit for development of backend http web applications with frontend clients
- compose several high-quality libraries to the best net-gain
- to support operational and developer workflows out of the box (testing, ci builds, etc)
- to enable production ready requirements (logging, monitoring, docker, builds, etc)

amigo-starter is built on two sister projects

- [amigo-dev](https://gitlab.com/digiresilience/link/amigo-dev) - provides a bundle of `devDependencies` and configuration for eslint, bable, tsc, etc.
- [amigo](https://gitlab.com/digiresilience/link/amigo) - provides runtime utils like logging, monitoring, etc (inspired by python's
  [talisker](https://talisker.readthedocs.io/en/latest/overview.html))

## Features

- **Configuration**

  Define your runtime config, load it automatically, and access it at runtime with type safety

  Configuration can be loaded via config file and/or environment variables.

  All config options have sane default values when possible.

  Get help on the available config options with `./cli config-help`. Oh and you
  can generate a default config file if you want one with `./cli config-generate`

  (thanks to mozilla's [convict](https://github.com/mozilla/node-convict/))

- **Sane development defaults**

  Typescript, eslint, jest, prettier, and babel are all configured out of the
  box. Updates are centrally managed through
  [amigo-dev](https://gitlab.com/digiresilience/link/amigo-dev), but can be
  overriden locally.

- **Monitoring**

  Exporting metrics from the api server in prometheus format comes out of the
  box on port 3002:/metrics. You can easily expose your own custom metrics.

  See [@app/api/src/app/routes/random/index.ts](@app/api/src/app/routes/random/index.ts) for an example.

  (thanks to [promster](https://github.com/tdeekens/promster))

- **Logging**

  High-speed and pre-configured JSON logging out of the box. Pino is a bit
  different in that in expects the first parameter to be data if you have any,
  the second param to be your message (in printf style), and every other param
  to be printf args.

  See [@app/api/src/app/routes/random/index.ts](@app/api/src/app/routes/random/index.ts) for an example.

  (thanks to [pino](https://github.com/pinojs/pino))

- **Database**

  Postgresql pre-configured with generated typescript interfaces, immutable migrations and
  a simple data access layer.

  No ORM - We assume you build from your postgresql schema up, not the other way around.
  Use the full power of postgresql. But don't waste time writing CRUD queries,
  we provide a simple CRUD adapter for tables (that supports composite id types!)

  (thanks to [pg-promise](https://github.com/vitaly-t/pg-promise),
  [graphile-migrate](https://github.com/graphile/migrate))

- **Background job processing**

  Process your background jobs with the built in queue. Use `worker-utils` to
  easily add a job from the backend. Run the worker with `make worker`.

  See [@app/api/src/app/index.ts](@app/api/src/app/index.ts) for an example.

  (thanks to [graphile-worker](https://github.com/graphile/migrate)

- **CRUD HTTP endpoints**

  Simple CRUD endpoints for your postgresql tables for rapid prototyping. Create
  your table, add the typescript record, and boom, you've got a functioning CRUD
  API.

  (see amigo's [CrudController](https://gitlab.com/digiresilience/link/amigo/-/blob/master/src/controllers/crud-controller.ts)
  and [CrudRepository](https://gitlab.com/digiresilience/link/amigo/-/blob/master/src/records/crud-repository.ts)
  for the implementation)

- **API Documentation**

  Swagger documentation for your api endpoints is automatically generated at `/api-docs`.

- **Next.js**

  All the benefits of [Next.js](https://nextjs.org/).. 'nuff said.

- **Built in Authentication and User management**

  Next-auth lets us login easily with cloudflare access or any one of a dozen or
  more providers. See the Authentication section below for more information.

  Username+password authentication is not supported (but you could add it).

  (thanks to [next-auth](https://next-auth.js.org))

## Dependencies

1. Node LTS
2. Docker + docker-compose
3. yarn
4. make
5. jq

## Quickstart

1.  Clone the repository

        git clone https://gitlab.com/digiresilience/link/amigo-starter.git YOUR_PROJECT_NAME
        cd YOUR_PROJECT_NAME
        rm -rf .git
        git init

2.  Build the application and prepare the database

        make db/start
        make build

3.  Run the application

        cp amigo.local.json.example .amigo.local.json
        vim .amigo.local.json      # add your config
        make api                   # run the api http server
        make worker                # run the background processor (in another terminal)
        make frontend              # run the frontend

Open `http://localhost:3000` to see the frontend.
Open `http://localhost:3001/api-docs` to see the auto generated api documentation.
Open `http://localhost:3002/metrics` to see the exported prometheus metrics.

## Usage

We use yarn workspaces to simplify working with this mono repo. Different parts
of the system are expressed as packages. They can depend on eachother and import
them as if they were normal packages.

See the project source code for detailed examples and usage information. Each
sub-package has a README with more information.

Generally you should use the well built Makefile to drive your development,
rather than calling `yarn` commands directly. The exception to this is the yarn
workspace commands outlined in the next section.

Peruse the Makefile to discover what's possible. Your daily driver make commands are:

| make target       | description                                                                          |
| ----------------- | ------------------------------------------------------------------------------------ |
| `build`           | builds the whole project                                                             |
| `fmt`             | runs the formatter across the whole project                                          |
| `test`            | runs the test suite and linters across the whole project                             |
| `cypress`         | runs the end-to-end tests interactively (the api server and frontend must be running |
| `watch`           | like, `build` but watches for changes                                                |
| `frontend`        | run the frontend server                                                              |
| `api`             | run the api server                                                                   |
| `worker`          | run the worker server                                                                |
| `list`            | lists the packages our app is made from                                              |
| `$PKG/build`      | builds a single package                                                              |
| `$PKG/test`       | run the test suite for a single package                                              |
| `$PKG/watch-test` | runs the test suite for a single package and watches for changes (only: api, worker) |
| `$PKG/fmt`        | run the formatters for a single package                                              |
| `db/start`        | start the development database containers                                            |
| `db/stop`         | stop the development database containers                                             |
| `db/down `        | remove the development database containers (️⚠️ delete's all data!)                  |
| `db/reset-docker` | resets the containers (️⚠️ delete's all data!)                                       |
| `db/current`      | applies the current migration once                                                   |
| `db/watch`        | watches the current migration and applies it when it changes                         |
| `db/commit`       | commit the current migration as a new fixed migration                                |
| `db/uncommit`     | undo a `db/commit`                                                                   |
| `clean`           | cleans up all build, testing artifacts, and docker dev containers                    |
| `distclean`       | like `clean` but also removes node_modules                                           |

To get OAuth authentication for development copy the `amigo.local.json.example`
to `.amigo.local.json`, and fill in your credentials. Remove unused providers.

You can specify the location of the config file at runtime with `AMIGO_CONFIG`
environment variable. Or you can specify the entire config via environment
variables if you wish.

You can run the graphile-migrate cli tool to manage migrations with `./cli db -- <graphile-migrate commands here>`, for example:

    ./cli db -- --help        # print graphile-migrate's help
    ./cli db -- watch --once  # apply the db/migrations/current.sql once (same as make db/watch)

The `--` escape is necessary to prevent the cli tool from eating the `--flags`
that should be passed to graphile-migrate. Do not run `graphile-migrate` directly, as the convict configuration will not be
loaded.

### Yarn workspaces cheat sheet

Print info about the current workspaces and their dependencies:

```console
$ yarn workspaces info
```

Run a command in a workspace

```console
$ yarn workspace <package-name> <command>

# examples

$ yarn workspace @app/frontend add next
$ yarn workspace @app/frontened add @types/react -D
```

Add a common dependency to all packages:

```console
$ yarn add some-package --ignore-workspace-root-check
```

Add a in-tree dependency to another in-tree project. You must specify a version
number (the one in the extant package.json), otherwise yarn will search for the
package in the registry.

```console
$ yarn workspace @app/api add @app/config@0.0.1
```

### Authentication

Amigo starter does not support user/password based authentication. It only supports oauth.

There are two oauth mechanisms supported:

1. direct next-auth via a [builtin provider](https://next-auth.js.org/configuration/providers)
2. cloudflare access via our custom next-auth shim

They are mutually exclusive. If cloudflare access config values are provided, it takes priority.

### Direct next-auth

Configure your provider(s) in the `nextAuth` key of the application config. See .amigo.local.json

You must supply a secret key/passphrase for `nextAuth.secret` and base 64 encoded JWT signing and encryption key. You can generate the latter with `./cli gen-jwks`/

### Cloudflare access

Configure the `cfaccess` key of the application config. Any nextAuth providers are ignored. The `nextAuth.secret` and signing+encryption keys are still required.

When cloudflare access is configured, all other providers are ignored and
authentication is only possible with cloudflare access. This means you will need
to do local development with cloudflare tunnel.

### Testing

Jest is used in the api and worker for small integration and unit tests.

- [Jest expect matchers](https://jestjs.io/docs/en/expect)

Cypress is used for full end-to-end tests that drive the client side app in a
browser, and touch the api server too.

- [Getting started with cypress](https://docs.cypress.io/guides/getting-started/writing-your-first-test.html#Write-your-first-test)
- [Assertions](https://docs.cypress.io/guides/references/assertions.html)
- [Examples](https://docs.cypress.io/guides/references/assertions.html)

## Credits

Copyright © 2020-present [Center for Digital Resilience][cdr]

### Contributors

| [![Abel Luck][abelxluck_avatar]][abelxluck_homepage]<br/>[Abel Luck][abelxluck_homepage] |
| ---------------------------------------------------------------------------------------- |

[abelxluck_homepage]: https://gitlab.com/abelxluck
[abelxluck_avatar]: https://secure.gravatar.com/avatar/0f605397e0ead93a68e1be26dc26481a?s=100&d=identicon

### License

[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0.en.html)

    GNU AFFERO GENERAL PUBLIC LICENSE
    Version 3, 19 November 2007

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

[cdrtech]: https://digiresilience.org/tech/
[cdr]: https://digiresilience.org
